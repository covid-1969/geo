from fastapi import FastAPI
from geographiclib import geodesic

app = FastAPI()


@app.get("/circle/{lat}/{lon}/{radius}")
def read_root(lat: float, lon: float, radius: int):
    coordinates = []
    for degree in range(360):
        if degree % 10 == 0:
            result = geodesic.Geodesic.WGS84.Direct(
                lat, lon, degree, radius)
            lat2, lon2 = result["lat2"], result["lon2"]
            coordinates.append([lon2, lat2])       
    if len(coordinates) > 2:
        coordinates.append(coordinates[0])
    return {
        'type': 'Polygon',
        'coordinates': [coordinates]
    }
